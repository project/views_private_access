 INTRODUCTION
 ------------ 
 Welcome to Views Private Access. An access plugin for Views.
 This module is designed to limit view access to only the UID specified in the URL path.

 INSTALLATION
 ------------
 Download the module from Drupal.org and enable it.

 REQUIREMENTS
 ------------
 As this module creates a new Views access plugin, Views is the only requirement.

 USAGE
 ------------
 The plugin is designed to keep any view with a path containing the user id wildcard
 to private. After enabling the module, when editing a view, you are presented with a new 'Private'
 access selection. So long as the first wildcard in the view path is the UID the plugin will compare that
 value to the currently logged in user. It also makes a check for an admin permission, which overrides
 the check.

 CREDITS
 ------------
 A version of this code was originally posted to the issue https://drupal.org/node/305250 
 and modified by my company, EthosCE (www.ethosce.com) for use in Drupal 6.

 Current Maintainer: Raymond Cascella <raymondcascella@gmail.com>

 Original code contributions by:
 guillaumev https://drupal.org/user/849890
 duellj https://drupal.org/user/168159
 